﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient
{
    public class LinqService : BaseService
    {
        public LinqService() : base() {
            Endpoint = "Linq";
        }
        public override Task<ICollection<T>> Get<T>()
        {
            return base.Get<T>();
        }
        public override Task<T> GetOne<T>()
        {
            return base.GetOne<T>();
        }
    }
}

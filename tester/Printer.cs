﻿using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient
{
    public static class Printer
    {
        public static void Print(List<TeamInfoDTO> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }


            foreach (var item in result)
            {
                Console.WriteLine($"Team id: {item.id.ToString()}");
                Console.WriteLine($"Team name: {item.name.ToString()}");

                Console.WriteLine($"Team users count {item.users.Count()}");

                foreach (var user in item.users)
                {
                    Console.WriteLine(user.ToString());
                }

            }
        }
        public static void Print(Dictionary<string, List<TaskDTO>> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Values.Count()}");

            foreach (var item in result)
            {
                Console.WriteLine($"User {item.Key.ToString()}");
                foreach (var task in item.Value)
                {
                    Console.WriteLine($"TaskNameLen:{task.name.Length} Tasks {task.ToString()}");
                }

            }
        }
        public static void Print(List<TaskIdName> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Count()}");

            foreach (var item in result)
            {
                Console.WriteLine($"Project {item.ToString()}");

            }
        }
        public static void Print(List<TaskDTO> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Count()}");

            foreach (var item in result)
            {
                Console.WriteLine($"Project {item.ToString()}");

            }


        }
        public static void Print(Dictionary<ProjectDTO, int> result)
        {

            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Values.Sum()}");

            foreach (var item in result)
            {
                Console.WriteLine($"Project {item.Key.ToString()}");
                Console.WriteLine($"Count tasks {item.Value.ToString()}");

            }
        }
        internal static void Print(ICollection<dynamic> res)
        {
            if (res.Count == 0)
            {
                Console.WriteLine("No Data");
                return;
            }
            Console.WriteLine(res.FirstOrDefault());

        }
    }
}

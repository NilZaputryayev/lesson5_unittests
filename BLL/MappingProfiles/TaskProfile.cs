﻿using AutoMapper;
using Common.DTO.Task;
using DAL.Models;

namespace BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, UpdateTaskDTO>()
                .ReverseMap();
            CreateMap<Task, TaskDTO>()
                .ReverseMap();
            CreateMap<Task, NewTaskDTO>()
                .ReverseMap();
        }
    }
}

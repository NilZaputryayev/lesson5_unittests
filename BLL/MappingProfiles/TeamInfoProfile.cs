﻿using AutoMapper;
using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using DAL.Models;

namespace BLL.MappingProfiles
{
    public sealed class TeamInfoProfile : Profile
    {
        public TeamInfoProfile()
        {
            CreateMap<TeamInfo, TeamInfoDTO>();
        }
    }
}

﻿using AutoMapper;
using DAL;
using DAL.Context;

namespace BLL
{
    public abstract class BaseService
    {
        private protected readonly LinqEFContext _context;
        private protected readonly IMapper _mapper;

        public BaseService(LinqEFContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


    }

}
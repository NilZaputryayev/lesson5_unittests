﻿using Common.DTO.User;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL
{
    public interface IApiUserService
    {
        Task Delete(int id);
        Task<List<UserDTO>> Get();
        Task<UserDTO> GetByID(int id);
        Task Update(UpdateUserDTO user);
        Task<UserDTO> Create(NewUserDTO user);
    }
}
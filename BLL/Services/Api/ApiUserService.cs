﻿using AutoMapper;
using Common.DTO.User;
using DAL;
using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BLL
{
    public class ApiUserService : BaseService, IApiUserService
    {
        public ApiUserService(LinqEFContext context, IMapper mapper) : base (context, mapper)
        {
        }

        public async Task<List<UserDTO>> Get()
        {
            return _mapper.Map<List<UserDTO>>(await _context.Users.ToListAsync());
        }
        public async Task<UserDTO> GetByID(int id)
        {
            return _mapper.Map<UserDTO>(await _context.Users.FindAsync(id));
        }
        public async Task Update(UpdateUserDTO DTO)
        {
            var usr = await _context.Users.FindAsync(DTO.id);

            if(usr != null)
            {
                usr.firstName = DTO.firstName;
                usr.lastName = DTO.lastName;
                usr.email = DTO.email;
                usr.birthDay = DTO.birthDay;

                await _context.SaveChangesAsync();

            }
        }
        public async Task Delete(int id)
        {
            var entity = await _context.Users.FindAsync(id);
            if (entity == null) throw new ArgumentException();

            _context.Users.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<UserDTO> Create(NewUserDTO user)
        {
            var userModel = _mapper.Map<User>(user);

            var created = await _context.Users.AddAsync(userModel);

            var createdDTO = _mapper.Map<UserDTO>(created.Entity);

            await _context.SaveChangesAsync();

            return createdDTO;


        }


    }
}

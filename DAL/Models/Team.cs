﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace DAL.Models
{

    public class Team
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string name { get; set; }
        public DateTime createdAt { get; set; }
        public IEnumerable<User> users { get; set; }
    }



}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;

namespace DAL.Models
{

    public class Task
    {
        [Required]
        public int id { get; set; }
        public int projectId { get; set; }
        public int performerId { get; set; }
        [Required]
        [MaxLength(250)]
        public string name { get; set; }
        public string description { get; set; }
        public int state { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime finishedAt { get; set; }
        public User Performer { get; set; }
        public string SampleColumn { get; set; }

    }



}

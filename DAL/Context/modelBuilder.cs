﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private static LoadService _loader = new LoadService();
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .Property(p => p.id)
            .ValueGeneratedNever();

            modelBuilder.Entity<Project>()
            .Property(p => p.id)
            .ValueGeneratedNever();

            modelBuilder.Entity<Task>()
            .Property(p => p.id)
            .ValueGeneratedNever();

            modelBuilder.Entity<Team>()
            .Property(p => p.id)
            .ValueGeneratedNever();           
            

        }

        public static void Seed(this ModelBuilder modelBuilder)
        {

            var projects = LoadDataFromAPI<Models.Project>();//.Where(x=>x.id >0);
            var users = LoadDataFromAPI<Models.User>();//.Where(x => x.id > 0);
            var teams = LoadDataFromAPI<Models.Team>();//.Where(x => x.id > 0);
            var tasks = LoadDataFromAPI<Models.Task>();//.Where(x => x.id > 0);


            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<Task>().HasData(tasks);

        }

        private static List<T> LoadDataFromAPI<T>()
        {
            _loader.Endpoint = $"{typeof(T).ToString().Split('.')[2]}s";
            return _loader.Get<T>().Result;
        }


    }
}

﻿using BLL;
using Common.DTO.Project;
using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class ProjectController : ControllerBase
    {
        private readonly IApiProjectService _ProjectService;

        public ProjectController(IApiProjectService ProjectService)
        {
            _ProjectService = ProjectService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var data = await _ProjectService.GetAsync();
            return Ok(data);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetAsync(int id)
        {
            var entity = await _ProjectService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateProjectDTO ProjectDTO)
        {
            await _ProjectService.Update(ProjectDTO);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync(NewProjectDTO ProjectDTO)
        {          

            return  Ok(await _ProjectService.Create(ProjectDTO));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _ProjectService.Delete(id);
            return NoContent();
        }
    }
}

﻿using BLL;
using Common.DTO.Team;
using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/[controller]")]


    public class TeamController : ControllerBase
    {
        private readonly IApiTeamService _TeamService;

        public TeamController(IApiTeamService TeamService)
        {
            _TeamService = TeamService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            return Ok(await _TeamService.Get());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var entity = await _TeamService.GetByID(id);

            if (entity == null) return NotFound(new { message = "not found" });

            return Ok(entity);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateTeamDTO TeamDTO)
        {
            await _TeamService.Update(TeamDTO);
            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> CreateAsync(NewTeamDTO TeamDTO)
        {          

            return Ok(await _TeamService.Create(TeamDTO));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _TeamService.Delete(id);
            return NoContent();
        }
    }
}

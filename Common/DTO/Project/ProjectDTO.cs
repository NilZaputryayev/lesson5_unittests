﻿using Common.DTO.Task;
using Common.DTO.Team;
using Common.DTO.User;
using System;
using System.Collections.Generic;

namespace Common.DTO.Project
{
    public class ProjectDTO
    {
        public int id { get; set; }
        public int authorId { get; set; }
        public int teamId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime deadline { get; set; }

        public override string ToString()
        {
            return $"id: {id}, authorId:{authorId}, teamId:{teamId}, name:{name}, description: {description}, created:{createdAt.ToShortDateString()}, deadline:{deadline.ToShortDateString()} ";
        }


    }
}
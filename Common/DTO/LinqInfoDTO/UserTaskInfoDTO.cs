﻿using Common.DTO.Project;
using Common.DTO.Task;
using Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    public class UserTasksInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int CountCancelOrUnfinishedTasks { get; set; }
        public TaskDTO LongestTask { get; set; }

        public void Print()
        {
            Console.WriteLine($"User:\n\r{User.ToString()}");
            Console.WriteLine($"LastProject:\n\r{LastProject.ToString()}");
            Console.WriteLine($"LastProjectTasksCount: {LastProjectTasksCount}");
            Console.WriteLine($"CountCancelOrUnfinishedTasks: {CountCancelOrUnfinishedTasks}");
            Console.WriteLine($"LongestTask:\n\r{LongestTask.ToString()}");
        }
    }
}

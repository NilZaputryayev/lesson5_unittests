﻿using Common.DTO.Project;
using Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Test.Integrational;
using Xunit;

namespace WebAPI.Tests.Integrational
{
    public class UserControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public UserControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateDefaultClient();
        }

        public async Task<int> GetUserCount()
        {
            var httpResponse = await _client.GetAsync($"user/");

            return (JsonSerializer.Deserialize<List<UserDTO>>(await httpResponse.Content.ReadAsStringAsync()).Count());

        }

        [Fact]
        public async Task DeleteAsync_WhenDeleteUser_UserActualyDeleted()
        {
            var userID = 1;

            var userCountBefore = await GetUserCount();

            var httpResponse = await _client.DeleteAsync($"user/{userID}");

            var usercountAfter = await GetUserCount();

            Assert.Equal(System.Net.HttpStatusCode.NoContent, httpResponse.StatusCode);
            Assert.Equal(1, userCountBefore - usercountAfter);

        }

        [Fact]
        public async Task DeleteAsync_WhenDeleteUserIDNotFound_ReturnNotFound()
        {
            var userID = 99999;

            var userCountBefore = await GetUserCount();

            var httpResponse = await _client.DeleteAsync($"user/{userID}");

            var usercountAfter = await GetUserCount();

            Assert.Equal(System.Net.HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal(0, userCountBefore - usercountAfter);

        }

        public void Dispose()
        {
        }
    }
}

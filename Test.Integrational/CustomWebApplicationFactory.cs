﻿using DAL.Context;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Net.Http;
using WebAPI;

namespace Test.Integrational
{
    public class CustomWebApplicationFactory<IStartup> : WebApplicationFactory<Startup>
    {


        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<LinqEFContext>));

                services.Remove(descriptor);

                services.AddDbContext<LinqEFContext>(options =>
                {
                    options.UseInMemoryDatabase("LinqEFDB");
                });


                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<LinqEFContext>();

                    db.Database.EnsureCreated();

                }
            });
        }

        protected override void ConfigureClient(HttpClient client)
        {
            client.BaseAddress = new Uri("https://localhost:5001/api/");
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");

        }
    }
}

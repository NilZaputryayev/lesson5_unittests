﻿using Common.DTO;
using Common.DTO.Project;
using Common.DTO.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Test.Integrational;
using Xunit;

namespace WebAPI.Tests.Integrational
{
    public class LinqControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public LinqControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateDefaultClient();
        }


        [Fact]
        public async Task GetProjectInfo_WhenWrongID_ThenReturnBadRequest()
        {
            var wrongFormatId = "abc";

            var httpResponse = await _client.GetAsync($"linq/GetProjectInfo/{wrongFormatId}");

            var res = JsonConvert.DeserializeObject<ProjectInfoDTO>(await httpResponse.Content.ReadAsStringAsync());

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetProjectInfo_WhenIDExist_ThenReturnProjectInfoDTO()
        {
            var id = 2;

            var testDescription = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.";

            var httpResponse = await _client.GetAsync($"linq/getprojectinfo/{id}");

            var res = JsonConvert.DeserializeObject<ProjectInfoDTO>(await httpResponse.Content.ReadAsStringAsync());


            Assert.Equal(System.Net.HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(testDescription, res.LongestTaskByDescription.description);
        }

        public void Dispose()
        {
        }
    }
}

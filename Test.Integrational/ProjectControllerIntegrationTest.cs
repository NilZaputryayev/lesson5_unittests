﻿using Common.DTO.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Test.Integrational;
using Xunit;

namespace WebAPI.Tests.Integrational
{
    public class ProjectControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public ProjectControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }


        [Fact]
        public async Task CreateAsync_WhenCreateProject_ReturnNewProject()
        {
            var project = new NewProjectDTO()
            {
                id = 9999,
                name = "test project",
                authorId = 1,
                createdAt = DateTime.Now,
                description = "new test project",
                teamId = 1
            };

            var json = JsonSerializer.Serialize(project);

            var httpResponse = await _client.PostAsync("api/project", new StringContent(json, Encoding.UTF8, "application/json"));

            var createdProject = JsonSerializer.Deserialize<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync());


            Assert.Equal(System.Net.HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(typeof(ProjectDTO), createdProject.GetType());
            Assert.Equal(9999, createdProject.id);

        }

        public void Dispose()
        {
        }
    }
}

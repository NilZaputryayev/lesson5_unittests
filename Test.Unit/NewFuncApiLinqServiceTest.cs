﻿using AutoMapper;
using BLL.MappingProfiles;
using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using DAL;
using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BLL
{
    public class NewFuncApiLinqServiceTest : IClassFixture<DbFixture>, IDisposable
    {
        readonly ApiLinqService apiLinqService;
        private IMapper _mapper;
        private LinqEFContext _context;

        private DbFixture _dbFixture; 


        public NewFuncApiLinqServiceTest()
        {
            _dbFixture = new DbFixture();
            ConfigService();
            apiLinqService = new ApiLinqService(_context,_mapper);
        }

        private void ConfigService()
        {
            
            _mapper = _dbFixture.GetMapper();
            _context = _dbFixture.CreateContext();
        }

        public void Dispose()
        {
            _dbFixture.Dispose();
        }

       

        [Theory]
        [InlineData(35, 4)]
        [InlineData(28, 3)]
        public void GetUnfinishedUserTasks_WhenID_ThenReturnExpectedCount(int id, int expectedCount)
        {

            var result = apiLinqService.GetUserUnfinishedUserTasksByUserID(id);

            Assert.Equal(expectedCount, result.Count());


        }
       


    }
}

﻿using AutoMapper;
using BLL.MappingProfiles;
using DAL.Context;
using DAL.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;

namespace BLL
{
    public class DbFixture : IDisposable
    {
        public DbFixture()
        {
        }

        private void Seed()
        {
            using var context = new LinqEFContext(new DbContextOptionsBuilder<LinqEFContext>()
                                        .UseInMemoryDatabase(databaseName: "LinqEFDB")
                                        .Options);

            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            //var user = new User()
            //{
            //    id = 2,
            //    email = "user1@google.com",
            //    firstName = "Jack",
            //    lastName = "Black",
            //    birthDay = new DateTime(1990, 01, 01),

            //};

            //var team = new Team()
            //{
            //    id = 1,
            //    name = "Dinamo",
            //    users = new List<User> { user }
            //};

            //var task = new Task()
            //{
            //    id = 1,
            //    name = "Task 1",
            //    state = 1,
            //    createdAt = new DateTime(2021, 01, 01),
            //    description = "Task 1",
            //    Performer = user

            //};

            //var project = new Project()
            //{
            //    id = 1,
            //    Author = user,
            //    createdAt = new DateTime(2021, 01, 01),
            //    deadline = new DateTime(2022, 1, 1),
            //    description = "Project 1 Test description",
            //    name = "Project 1",
            //    teamId = 1,
            //    Tasks = new List<Task>() { task }
            //};





            //context.Add(team);

            //context.SaveChanges();
        }

        public LinqEFContext CreateContext()
        {
            var context = new LinqEFContext(new DbContextOptionsBuilder<LinqEFContext>()
                .UseInMemoryDatabase(databaseName: "LinqEFDB")
                .Options);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

         //   Seed();

            return context;
        }

        public IMapper GetMapper()
        {
            var configuration = new MapperConfiguration(
                cfg =>
                {
                    cfg.AddProfile<UserProfile>();
                    cfg.AddProfile<TaskProfile>();
                    cfg.AddProfile<TeamProfile>();
                    cfg.AddProfile<ProjectProfile>();
                    cfg.AddProfile<ProjectInfoProfile>();
                    cfg.AddProfile<UserTasksInfoProfile>();
                    cfg.AddProfile<TeamInfoProfile>();
                }
                );
            return new Mapper(configuration);
        }


        public void Dispose()
        {
            Console.WriteLine("SomeFixture: Disposing SomeFixture");
        }
    }
}

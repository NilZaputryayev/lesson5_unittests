﻿using AutoMapper;
using BLL.MappingProfiles;
using BLL.Models;
using Common.DTO;
using Common.DTO.Project;
using Common.DTO.Task;
using DAL;
using DAL.Context;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BLL
{
    public class ApiLinqServiceTest : IClassFixture<DbFixture>, IDisposable
    {
        readonly ApiLinqService apiLinqService;
        private IMapper _mapper;
        private LinqEFContext _context;
        private DbFixture _dbFixture;


        public ApiLinqServiceTest()
        {
            _dbFixture = new DbFixture();
            ConfigService();

            apiLinqService = new ApiLinqService(_context, _mapper);
        }

        private void ConfigService()
        {
            _mapper = _dbFixture.GetMapper();
            _context = _dbFixture.CreateContext();
        }

        public void Dispose()
        {
            _dbFixture.Dispose();
        }

        [Fact]
        public void GetUserTaskCountDictionaryByID_WhenIDEqual35_ThenReturn6()
        {
            int id = 35;
            int countUserTasks = 6;

            Assert.Equal(countUserTasks, apiLinqService.GetUserTaskCountDictionaryByID(id).Count());
        }

        [Theory]
        [InlineData(62, 3)]
        [InlineData(45, 1)]
        [InlineData(28, 1)]
        [InlineData(20, 2)]
        [InlineData(66, 1)]
        public void TaskListFinished2021ByID_WhenIDEqualID_ThenReturnExpectedCount(int id, int count)
        {

            Assert.Equal(count, apiLinqService.TaskListFinished2021ByID(id).Count());

        }

        [Theory]
        [InlineData(20, 6)]
        [InlineData(21, 8)]
        [InlineData(22, 7)]
        public void ListTasksWith45NameByID_WhenIDEqual_ThenReturnExpected(int id, int countTasks)
        {

            var tasks = apiLinqService.ListTasksWith45NameByID(id);

            Assert.Equal(countTasks, apiLinqService.ListTasksWith45NameByID(id).Count());

        }

        [Fact]
        public void ListTasksWith45NameByID_WhenIDEqual23_ThenLenOfEveryTaskNameLess45()
        {
            int id = 23;

            Assert.True(apiLinqService.ListTasksWith45NameByID(id).All(x => x.name.Length < 45));

        }


        [Fact]
        public void GetUserListSortedWithTasks_WhenGetList_ThenMustBeenSortedByName()
        {
            var userList = apiLinqService.GetUserListSortedWithTasks().Keys.ToList();
            var userListSorted = userList.OrderBy(x => x).ToList();

            Assert.Equal(userList, userListSorted);
        }


        [Fact]
        public void GetTeamsOlder10SortedAndGrouped_WhenGetList_ThenMustBeSorted()
        {
            var data = apiLinqService.GetTeamsOlder10SortedAndGrouped().ToList();
            var dataSorted = data.OrderBy(x => x.name).ToList();

            Assert.Equal(data, dataSorted);

        }

        [Fact]
        public void GetTeamsOlder10SortedAndGrouped_WhenGetList_ThenAllUsersMustBeOlder10()
        {
            var allUsersOlder10 = apiLinqService.GetTeamsOlder10SortedAndGrouped().All(x => x.users.All(y => DateTime.Now.Year - 10 > y.birthDay.Year));

            Assert.True(allUsersOlder10);

        }

        [Fact]
        public void GetUserTasksInfo_WhenID28_ThenLastProjectNameIsWooden()
        {
            int id = 28;

            Assert.Equal("Wooden", apiLinqService.GetUserTasksInfo(id).LastProject.name);
        }


        [Fact]
        public void GetUserTasksInfo_WhenNoUser_ThenReturnNull()
        {
            int id = 999;
            Assert.Null(apiLinqService.GetUserTasksInfo(id));

        }


        [Fact]
        public void GetProjectInfo_WhenID0_ThenLastProjectNameIsIndex()
        {

            Assert.Equal("index", apiLinqService.GetProjectInfo(0).ShortestTaskByName.name);

        }





    }
}

